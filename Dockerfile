FROM bitnami/jasperreports:7.8.1-debian-10-r99
# change to root to install plugins
USER root

RUN mkdir -p /bitnami/jasperreports-mounted-conf/lib

COPY ./plugins/jaspersoft_webserviceds_v1.5/JRS/WEB-INF/lib/*.jar /bitnami/jasperreports-mounted-conf/lib/

RUN mkdir -p /bitnami/jasperreports-mounted-conf/bundles

COPY ./plugins/jaspersoft_webserviceds_v1.5/JRS/WEB-INF/bundles/webserviceds.properties /bitnami/jasperreports-mounted-conf/bundles/

COPY ./plugins/jaspersoft_webserviceds_v1.5/JRS/WEB-INF/applicationContext-WebServiceDataSource.xml /bitnami/jasperreports-mounted-conf/

COPY ./plugins/org.universis.number-format-1.0.2/WEB-INF/lib/org.universis.number-format.bundled.jar /bitnami/jasperreports-mounted-conf/lib/
# change back to jasperreports user
USER 1001