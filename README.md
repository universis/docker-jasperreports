# Docker image for jasperreports including web service data source


## Usage

JasperReports requires access to a MySQL or MariaDB database to store information. 

A docker network must be created in order to allow the JasperReports container to access the MariaDB container. To create the network, run the following command:

```bash
docker network create reports-network
```

Create a new container for MariaDB:

```bash
docker run -d --name reports-db \
  --env ALLOW_EMPTY_PASSWORD=yes \
  --env MARIADB_USER=bn_jasperreports \
  --env MARIADB_PASSWORD=bitnami \
  --env MARIADB_DATABASE=bitnami_jasperreports \
  --network reports-network \
  --network-alias mariadb \
  bitnami/mariadb:latest
```

    
Finally, create a new container for JasperReports:
    
```bash
  docker run -d --name reports-server \
  --env ALLOW_EMPTY_PASSWORD=yes \
  --env JASPERREPORTS_DATABASE_HOST=mariadb \
  --env JASPERREPORTS_DATABASE_PORT_NUMBER=3306 \
  --env JASPERREPORTS_DATABASE_USER=bn_jasperreports \
  --env JASPERREPORTS_DATABASE_PASSWORD=bitnami \
  --env JASPERREPORTS_DATABASE_NAME=bitnami_jasperreports \
  --network reports-network \
  registry3.it.auth.gr/universis-jasperreports:latest
```

Access your jasperserver at http://your-ip:8080/jasperserver/. The default username is `jasperadmin` and the default password is `bitnami`.

## Build the image
```bash
docker build -t universis/jasperreports:latest .
```